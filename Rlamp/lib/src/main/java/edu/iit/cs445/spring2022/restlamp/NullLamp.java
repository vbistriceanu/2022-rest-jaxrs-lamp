package edu.iit.cs445.spring2022.restlamp;

public class NullLamp extends Lamp {
    @Override
    public boolean isNil() {
        return true;
    }
}
