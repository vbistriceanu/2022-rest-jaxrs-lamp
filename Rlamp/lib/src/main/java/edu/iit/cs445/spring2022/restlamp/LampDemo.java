package edu.iit.cs445.spring2022.restlamp;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("api")
public class LampDemo extends Application {
	// Intentionally left empty for this demo
}
